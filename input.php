<?php
$dbhost = 'localhost';
$dbname = 'joboffer';
$dbuser = 'root';
$dbpass = '';
mysql_connect($dbhost, $dbuser, $dbpass);
mysql_select_db($dbname);

require_once('libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER["DOCUMENT_ROOT"].'/joboffer/templates');

$xml = file_get_contents('php://input');
$xml = simplexml_load_string($xml);
if($xml->action == 'getCategories')
{
    $query = mysql_query("select c.category_name, c.category_id,
        count(p.product_id) as products
        from category c
        left join product p on p.category_id = c.category_id
        group by p.category_id ORDER By c.category_id");
    while($row = mysql_fetch_object($query)) 
    {
        $cats[] = $row;
    }
    $smarty->assign('cats', $cats);
    $smarty->display('categories.tpl');
} else if($xml->action == 'getProducts') {
        $query = mysql_query("SELECT * FROM product WHERE category_id = 1 ORDER BY product_id");
    while($row = mysql_fetch_object($query)) 
    {
        $row->pvm = $row->price - round($row->price*100/121, 2);
        $prod[] = $row;
    }
    $smarty->assign('products', $prod);
    $smarty->display('products.tpl');
}
